import React from "react"
import { SwiperItemType } from "../../utils/types"
import "./swiperItem.scss"

export type propsTypes = SwiperItemType

function SwiperItem({ imageSrc, imageAlt }: propsTypes) {
  return (
    <li className="swiper-item">
      <img src={imageSrc} alt={imageAlt} className="swiper-img" />
    </li>
  )
}

export default SwiperItem
