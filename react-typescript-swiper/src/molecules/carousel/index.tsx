import { useEffect, useState } from "react"
import SwiperItem from "../../atoms/swiperItem/swiperItem"
import { SwiperItemType } from "../../utils/types"
import next from "../../utils/Icons/next.png"
import prev from "../../utils/Icons/prev.png"
import play from "../../utils/Icons/play.svg"
import pause from "../../utils/Icons/pause.svg"

import "./carousel.scss"

export type Props = {
  items: Array<SwiperItemType>
}

export default function Carousel({ items }: Props) {
  const [currentIndex, setCurrentIndex] = useState(0)
  const [playPause, setPlayPause] = useState("pause")
  const [tabIndex, setTabIndex] = useState(-1)
  const prevClicked = () => {
    if (currentIndex < 0) {
      return setCurrentIndex(3)
    }
    return setCurrentIndex(currentIndex - 1)
  }
  const nextClicked = () => {
    if (currentIndex > items.length - 2) {
      return setCurrentIndex(0)
    }
    return setCurrentIndex(currentIndex + 1)
  }

  const tabSelected = (slideNumber: number) => {
    setCurrentIndex(slideNumber - 1)
  }
  const tabSelectedKeys = (slideNumber: number) => {
    setCurrentIndex(slideNumber)
  }
  useEffect(() => {
    if (playPause === "play") {
      const slideInterval = setInterval(() => {
        setCurrentIndex((currentIndex) => (currentIndex < items.length - 1 ? currentIndex + 1 : 0))
      }, 3000)
      return () => clearInterval(slideInterval)
    }
  }, [playPause])
  return (
    <section id="my Carousel" className="swiper-container">
      <h1 id="carousel" tabIndex={0}>
        Carousel AccessAbility
      </h1>
      <div
        className="playPauseButton"
        aria-label="play pause button"
        role="play pause"
        tabIndex={0}
        onKeyPress={() => {
          playPause === "pause" ? setPlayPause("play") : setPlayPause("pause")
        }}>
        {playPause === "pause" ? (
          <img
            src={play}
            className="play"
            alt="pause"
            onClick={() => {
              setPlayPause("play")
            }}
            aria-label="play"
          />
        ) : (
          <img
            className="pause"
            src={pause}
            alt="play"
            onClick={() => {
              setPlayPause("pause")
            }}
            aria-label="pause"
          />
        )}
      </div>
      <ul
        className="swiper-list"
        style={{ transform: `translate(-${currentIndex * 100}%)`, transition: "ease all 0.5s" }}>
        {items.map((item, idx) => (
          <SwiperItem key={idx} {...item} />
        ))}
      </ul>
      <div id="previousNextButton" className="previousNext">
        <img
          src={prev}
          alt="previousButton"
          className="prevButton"
          onClick={prevClicked}
          tabIndex={0}
          onKeyPress={(e) => {
            if (e.key === "Enter") {
              prevClicked()
            }
          }}
        />
        <img
          src={next}
          alt="nextButton"
          className="nextButton"
          onClick={nextClicked}
          tabIndex={0}
          onKeyPress={(e) => {
            if (e.key === "Enter") {
              nextClicked()
            }
          }}
        />
      </div>
      <div
        className="tabWrapper"
        role="tablist"
        id="tabWrapper"
        aria-label="slides"
        tabIndex={0}
        onKeyDown={(e) => {
          if (e.key === "ArrowRight") {
            tabSelectedKeys(currentIndex)
            document.getElementById(`carousel-tab-${currentIndex + 1}`)?.focus()
          }
        }}>
        <button
          className={currentIndex === 0 ? "tabSelected" : "tab"}
          id="carousel-tab-1"
          type="button"
          role="tab"
          tabIndex={-1}
          aria-label="Slide 1"
          aria-selected={false}
          aria-activedescendant="carousel-tab-1"
          aria-controls="carousel-item-1"
          onClick={() => tabSelected(1)}
          onKeyDown={(e) => {
            if (e.key === "ArrowDown") {
              tabSelectedKeys(1)
              document.getElementById("carousel-tab-2")?.focus()
            }
            if (e.key === "ArrowLeft") {
              tabSelectedKeys(3)
              document.getElementById("carousel-tab-4")?.focus()
            }
          }}
        />
        <button
          className={currentIndex === 1 ? "tabSelected" : "tab"}
          id="carousel-tab-2"
          type="button"
          role="tab"
          tabIndex={-1}
          aria-label="Slide 2"
          aria-selected={false}
          aria-controls="carousel-item-2"
          onClick={() => tabSelected(2)}
          onKeyDown={(e) => {
            if (e.key === "ArrowDown") {
              tabSelectedKeys(2)
              document.getElementById("carousel-tab-3")?.focus()
            }
            if (e.key === "ArrowLeft") {
              tabSelectedKeys(0)
              document.getElementById("carousel-tab-1")?.focus()
            }
          }}
        />
        <button
          className={currentIndex === 2 ? "tabSelected" : "tab"}
          id="carousel-tab-3"
          type="button"
          role="tab"
          tabIndex={-1}
          aria-label="Slide 3"
          aria-selected={false}
          aria-controls="carousel-item-3"
          onClick={() => tabSelected(3)}
          onKeyDown={(e) => {
            if (e.key === "ArrowDown") {
              tabSelectedKeys(3)
              document.getElementById("carousel-tab-4")?.focus()
            }
            if (e.key === "ArrowLeft") {
              tabSelectedKeys(1)
              document.getElementById("carousel-tab-2")?.focus()
            }
          }}
        />
        <button
          className={currentIndex === 3 ? "tabSelected" : "tab"}
          id="carousel-tab-4"
          type="button"
          role="tab"
          tabIndex={-1}
          aria-label="Slide 4"
          aria-selected={false}
          aria-controls="carousel-item-4"
          onClick={() => tabSelected(4)}
          onKeyDown={(e) => {
            if (e.key === "ArrowDown") {
              tabSelectedKeys(0)
              document.getElementById("carousel-tab-1")?.focus()
            }
            if (e.key === "ArrowLeft") {
              tabSelectedKeys(2)
              document.getElementById("carousel-tab-3")?.focus()
            }
          }}
        />
      </div>
    </section>
  )
}
