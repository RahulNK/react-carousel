import "./App.scss"
import Carousel from "./molecules/carousel"

function App() {
  const items = [
    {
      imageSrc:
        "https://us1-test-images.disco-api.com/2021/8/3/09ec1bbf-fcf3-46ba-94bc-4bd184f071c9.jpg",
      imageAlt: "A person's eye"
    },
    {
      imageSrc:
        "https://us1-test-images.disco-api.com/2021/5/5/d4df5159-b6b7-4814-968b-f08486667867.jpg",
      imageAlt: "A rock formation"
    },
    {
      imageSrc:
        "https://us1-test-images.disco-api.com/2021/5/5/64b0242e-97d2-4bab-b0ef-67e68c02f7e4.jpg",
      imageAlt: "Some flowers"
    },
    {
      imageSrc:
        "https://us1-test-images.disco-api.com/2021/3/19/0fb8be44-65f0-4571-84b4-315176494551.jpg",
      imageAlt: "An egyptian wall painting"
    }
  ]

  return <Carousel items={items}></Carousel>
}

export default App
